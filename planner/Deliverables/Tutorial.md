---
title: "ReleaseThis Tutorial"
author: "Troy M. Dinga"
date: "10/23/2015"
output: pdf_document
---

1. You'll first need to clone the repository containing our source code, found [here](https://bitbucket.org/hamiltona-allegheny/cs280f2015-lab06-team2), hosted on `bitbucket.org`.

2. After this, you'll need to add two items to your classpath using the terminal. The first is the `bin` directory found inside the `planner` directory at the top level of our repository, and the second is the `jcommander-1.48.jar` file in the `lib` directory (also found in `planner`).

To add these items to your class path, you'll type the following into your terminal's command line:
```shell
export CLASSPATH=/path/to/the/cloned/repository/planner/bin:/same/path/once/again/.../planner/lib/jcommander-1.48.jar:.
```

3. After doing this, you'll need to compile the java source files using the `javac` command or through `ant compile`. To use the first method, you'll move down through `src` and its package structure using the `cd` command until you reach the source files, `planner.java` and `Jcom.java`. At this point, type `javac *.java` into your terminal and hit Enter/Return. 

4. Now, you're ready to run the program! Anywhere in the `planner` directory, you'll type the command `java edu.allegheny.team2.planner` into your terminal. However, before hitting Enter/Return, you'll need to offer up some arguments - namely: a total fixed cost and a `.csv` file containing your requirements. You'll have a space after the `java` command, and then type in `-c integerValueforCost -f data.csv`. The data file specified is the one that we've provided, but you can replace it with whatever file you so desire.

5. The program is set up, you can run it successfully, and you're ready to get planning!