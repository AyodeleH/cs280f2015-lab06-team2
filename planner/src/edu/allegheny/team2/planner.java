/*
 * Laboratory Assignment Six
 * Next Release Planner *
 *
*/

package edu.allegheny.team2;
import com.beust.jcommander.*;
import java.io.*;
import java.util.*;

public class planner {

	public static void main(String[] args) {

		//Welcome message
		System.out.println("");
		System.out.println("Wecome to ReleaseThis! V1.0");
		System.out.println("");
		System.out.println("What should you release next version?");
		System.out.println("Hmm...");
		System.out.println("");

		//JCommander "stuff" so that the command line arguments can be used
		Jcom jc = new Jcom();
		JCommander jcmd = new JCommander (jc, args);	
		
		//Save the CSV file name and total fixed cost args as variables
		String file = jc.getFileName();
		double totalFC = jc.getTotalCost();

		//Parses the CSV file and returns the first row, the requirement names, in an ArrayList of strings
		ArrayList<String> reqs = new ArrayList<String>();
		reqs = parseFromCSV(file, 0);

		//Parse the CSV file and returns the second row, the costs, in an ArrayList of strings
		ArrayList<String> costs = new ArrayList<String>();
		costs = parseFromCSV(file, 1);

		//Parse the CSV file and returns the second row, the benefits, in an ArrayList of strings
		ArrayList<String> benefits = new ArrayList<String>();
		benefits = parseFromCSV(file, 2);

		// Test sysos
		// ----------------------------------
		/*System.out.println(reqs);
		System.out.println(costs);
		System.out.println(benefits);*/
		// ----------------------------------
		
		// Create a hastable so that the key-value pair is:
		// Req : Cost/Benefit Ratio
		HashMap<String, Double> cbRatios = new HashMap<String, Double>();
		
		// Create an ArrayList of Cost/Benefit Ratios... as opposed to above method
		//ArrayList<Double> cbRatios = new ArrayList<Double>();
		//cbRatios = populateLOR(costs,benefits);

		// Populate the cbRatio hashmap
		cbRatios = populateHM(reqs,costs,benefits);

		// Another test syso
		//System.out.println(cbRatios);
				
		// Now that the cbRation Hashtable has the requirements-cost/benefit ratio as a Key-Value pair...
		// It's time to sort them
		// The Cost/Benefit Ratio of > 1 are not ones we want, that means the cost is far greater than the benefit of adding
		// it into the next release.

		HashMap<String, Double> sortedCbRatios = sortCbRatios(cbRatios);

		// Test syso
		//System.out.println(sortedCbRatios);
		
		// Time to add up the costs and see which to Release!
		cbRatioReleaseReqs(sortedCbRatios, totalFC, costs, benefits);

		} //main

public static ArrayList<String> parseFromCSV(String fileName, Integer row)
{
	int i = row;
	String csvFile = fileName;
	BufferedReader br = null;
	String line = "";
	String splitBy = ",";
	ArrayList<String> al = new ArrayList<String>();

		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null)
			{
				String[] array = line.split(splitBy);
				al.add(array[i]);
			}
			br.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return al;
}

public static HashMap<String, Double> populateHM(ArrayList<String> reqs, ArrayList<String> costs, ArrayList<String> benefits)
{
	HashMap<String, Double> hm = new HashMap<String, Double>();
	ArrayList<String> r = reqs;
	ArrayList<String> c = costs;
	ArrayList<String> b = benefits;

	for(int i=0; i < reqs.size(); i++)
	{
		hm.put(r.get(i), (Double.valueOf(c.get(i)) / Double.valueOf(b.get(i))));
	}
	
	return hm;
}

public static HashMap<String, Double> sortCbRatios(HashMap<String, Double> cbRatios)
{
	// !!!
	// Knowing next to nothing about sorting HashMaps...
	// http://www.mkyong.com/java/how-to-sort-a-map-in-java/
	// Relied on learning how to from here, method was clean and easy to follow
	// !!!

	// Convert HashMap to LinkedList for sorting
	LinkedList<Map.Entry<String, Double>> ll = new LinkedList<Map.Entry<String, Double>>(cbRatios.entrySet());

	// Sorting time, using a comparator, comparing the VALUES
	Collections.sort(ll, new Comparator<Map.Entry<String, Double>>() {
		public int compare(Map.Entry<String, Double> v1, Map.Entry<String, Double> v2) {
			return (v1.getValue()).compareTo(v2.getValue());
		}
	});

	// Not that the list is sorted, make it a HashMap again
	HashMap<String, Double> sortedCbRatios = new LinkedHashMap<String, Double>();
	for (Iterator<Map.Entry<String, Double>> i = ll.iterator(); i.hasNext();)
	{
		Map.Entry<String, Double> entry = i.next();
		sortedCbRatios.put(entry.getKey(), entry.getValue());
	}

	return sortedCbRatios;
}

public static void cbRatioReleaseReqs(HashMap<String, Double> sortedCbRatios, double totalFC, ArrayList<String> costs, ArrayList<String> benefits)
{
	HashMap<String, Double> chosenRatios = new HashMap<String, Double>();
	double calculatedCost = 0;
	double totalBenefit = 0;
	int inc = 0;

	for (String key : sortedCbRatios.keySet())
	{
		calculatedCost += Double.parseDouble(costs.get(Integer.parseInt(key) - 1));
		totalBenefit += Double.parseDouble(benefits.get(Integer.parseInt(key) - 1));

		if (calculatedCost >= totalFC) 
		{
			calculatedCost = calculatedCost - Double.parseDouble(costs.get(Integer.parseInt(key) - 1));
			totalBenefit = totalBenefit - Double.parseDouble(benefits.get(Integer.parseInt(key) - 1));

		}

		else if (calculatedCost < totalFC) 
		{
			chosenRatios.put(key, sortedCbRatios.get(key));
		}
	}

	for (String k : chosenRatios.keySet())
	{
		System.out.println("Requirement "+k+" should be released - Cost = $"+Double.parseDouble(costs.get(Integer.parseInt(k) - 1))+" | Benefit =$"+Double.parseDouble(benefits.get(Integer.parseInt(k) - 1)));
	}

	System.out.println("It will cost you $"+calculatedCost+" to implement these requirements... Which will give you a monetary benefit of $"+totalBenefit+". You have $"+(totalFC - calculatedCost)+" left over!");
}

public static ArrayList<HashMap<String, Double>> sortListOfRatios(ArrayList<HashMap<String, Double>> listOfRatios)
{
	ArrayList<HashMap<String, Double>> al = listOfRatios;

	for(int i=0; i < al.size(); i++)
	{
		if (al.get(i).get(i) > al.get(i+1).get(i+1))
		{
			Collections.swap(al, i, i+1);
		}
	}

	return al;
}
	
} //class
