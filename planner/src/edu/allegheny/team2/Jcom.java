package edu.allegheny.team2;
import com.beust.jcommander.*;

public class Jcom {

	@Parameter (names = "-f", description = "CSV File Name", required = true)
	private String fileName;

	@Parameter (names = "-c", description = "Total Fixed Cost, Integer Value", required = true)
	private Integer totalCost;

	@Parameter (names = "-help", description = "Help Menu for those in need", help = true)
	private boolean help;

	public String getFileName()
	{
		return fileName;
	}

	public Integer getTotalCost()
	{
		return totalCost;
	}

	public boolean returnHelpMenu()
	{
		return help;
	}

}
