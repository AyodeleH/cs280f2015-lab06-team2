//
//
//
//

import org.junit.*;
import java.io.*;

public class PlannerTests {

	@Test
	public void testToSeeIfParsingFromCSVFileReturns()
	{
		planner testPlanner = new planner();
		String file = "data.csv";
		ArrayList<String> al = new ArrayList<String>();

		assertEquals("Parsing from CSV should return an Arraylist of Strings", al, planner.parseFromCSVFile(file, 0));
		
	}

	@Test
	public void testPopulateHashMapMethod()
	{
		planner testPlanner = new planner();
		ArrayList<String> mockReqs = new ArrayList<String>();
		mockReqs.add("1");

		ArrayList<String> mockCosts = new ArrayList<String>();
		mockCosts.add("1");

		ArrayList<String> mockBenefits = new ArrayList<String>();
		mockBenefits.add("1");
	}

}
